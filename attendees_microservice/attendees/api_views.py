from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AccountVO, Attendee, ConferenceVO

from common.json import ModelEncoder


class AttendeesListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        # Get the count of AccountVO objects with email equal to o.email
        # Return a dictionary with "has_account": True if count > 0
        # Otherwise, return a dictionary with "has_account": False
        count = AccountVO.objects.filter(email=o.email).count()
        if count > 0:
            return JsonResponse({"has_account": True})
        else:
            return JsonResponse({"has_account": False})


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=ConferenceVODetailEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            # THIS LINE IS ADDED
            conference_href = f"/api/conferences/{conference_vo_id}/"

            # THIS LINE CHANGES TO ConferenceVO and import_href
            conference = ConferenceVO.objects.get(import_href=conference_href)

            content["conference"] = conference

        # THIS CHANGES TO ConferenceVO
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_attendee(request, pk):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    if request.method == "GET":
        attendees = Attendee.objects.get(id=pk)
        return JsonResponse(
            {
                "name": attendees.name,
                "email": attendees.email,
                "company_name": attendees.company_name,
                "created": attendees.created,
                "conference": {
                    "name": attendees.conference.name,
                    "href": attendees.conference.get_api_url(),
                },
            }
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            conference = ConferenceVO.objects.get(id=content["conference"])
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        Attendee.objects.filter(id=pk).update(**content)
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
