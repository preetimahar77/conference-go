from django.contrib import admin

from .models import Attendee, Badge, Account, Conference


@admin.register(Attendee)
class AttendeeAdmin(admin.ModelAdmin):
    pass


@admin.register(Badge)
class BadgeAdmin(admin.ModelAdmin):
    pass


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass


@admin.register(Conference)
class ConferenceAdmin(admin.ModelAdmin):
    pass
