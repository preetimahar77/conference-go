from datetime import datetime
from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    """
    The User model is someone that logs in to administer the
    conference application, not someone that is attending or
    presenting at the conference.

    The custom user model for this project as advised by Django docs
    https://docs.djangoproject.com/en/4.0/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
    """

    email = models.EmailField(unique=True)


class Account(models.Model):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    is_active = models.BooleanField(default=False)
    updated = models.DateField(default=datetime.now())
