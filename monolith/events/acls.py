import json
import requests

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"per_page": 1, "query": [city, state]}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
    #  return {"picture_url": None}
    # location = json.loads(response.content)
    # location_url = picture_url


def get_weather_data(city, state):
    # us_iso_code = "ISO 3166-2:US"

    geo_url = f"https://api.openweathermap.org/geo/1.0/direct?query={city},{state},{country}&limit=5&appid={PEXELS_API_KEY}"
    # &appid={OPEN_WEATHER_API_KEY}"
    params = {"per_page": 1, "query": [city, state]}
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    print(city)
    print(state)

    print(geo_url)
    response_geo = requests.get(geo_url, params=params, headers=headers)
    print(response_geo.json)
    content = json.dumps(response_geo.content)
    lat = ""
    lon = ""
    print(lat)

    for cities in content:
        if cities["name"] == city:
            lat = cities["lat"]
            lon = cities["lon"]
            break
    weather_url = f"https://api.openweathermap.org/geo/data/2.5/weather?lat={coordinates['lat']}&lon={coordinates['lon']}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    response_weather = requests.get(weather_url)
    weather_data = json.loads(response_weather.content)
    temperature = str(weather_data["main"]["temp"]) + "F"
    description = weather_data["weather"][0]["description"]
    try:
        return {"temperature": temperature, "description": description}
    except temperature.DoesNotExist:
        return {"temperature": None}
